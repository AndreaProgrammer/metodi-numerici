import java.text.NumberFormat;
import java.text.DecimalFormat;

public class SuccessioneMalCondizionata
{
	private double p1, p2, p;
	private int it;
	NumberFormat formatter;
	
	public SuccessioneMalCondizionata()
	{
		p1 = 1.0D;
		p2 = 1.0D/3.0D;
		it = 2;
		formatter = new DecimalFormat("0.#");
		formatter.setMaximumFractionDigits(20);
		formatter.setMinimumFractionDigits(20);
	}
	
	public String step()
	{
		p = ((10.0D/3.0D) * p2) - p1;
		p1 = p2;
		p2 = p;
		it++;
		return this.toString();
	}
	
	public static void calcola(int j)
	{
		double p1 = 1D;
		double p2 = 1D/3D;
		if(j>0)
			System.out.println(1 + "\t" + p1);
		if(j>1)
			System.out.println(2 + "\t" + p2);
		for(int i = 3; i<=j; i++)
		{
			double p3 = ((10D/3D) * p2) - p1;
			System.out.println(i + "\t" + p3);
			p1 = p2;
			p2 = p3;
		}
	}
	
	public String toString()
	{	
		return it + "\t" + formatter.format(p);
	}
}