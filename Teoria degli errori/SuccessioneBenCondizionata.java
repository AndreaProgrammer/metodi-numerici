import java.text.NumberFormat;
import java.text.DecimalFormat;

public class SuccessioneBenCondizionata
{
	private double p;
	private int it;
	NumberFormat formatter;
	
	public SuccessioneBenCondizionata()
	{
		p = 1.0D;
		it = 1;
		formatter = new DecimalFormat("0.#");
		formatter.setMaximumFractionDigits(20);
		formatter.setMinimumFractionDigits(20);
	}
	
	public String step()
	{
		p = (1.0D/3.0D)*p;
		it++;
		return this.toString();
	}
	
	public static void calcola(int j)
	{
		double p = 1D;
		for(int i = 1; i<=j; i++, p = (1D/3D)*p)
			System.out.println(i + "\t" + p);
	}
	
	public String toString()
	{
		return it + "\t" + formatter.format(p);
	}
}