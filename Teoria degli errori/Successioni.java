import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Successioni
{	
	public static void main(String args[])
	{
		System.out.print("Inserisci il numero di iterazioni: ");
		int it = 0;
		while(true)
		{
			try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in)))
			{
				it = Integer.parseInt(br.readLine());
				if(it<1)
					throw new Exception();
				break;
			}
			catch(Exception e)
			{
				System.out.println("Inserisci un valore valido");
			}
		}
		
		SuccessioneBenCondizionata sbc = new SuccessioneBenCondizionata();
		SuccessioneMalCondizionata smc = new SuccessioneMalCondizionata();
		
		System.out.println(sbc + "\t- " + sbc);
		for(int i = 1;i < it; i++)
			if(i == 1)
				System.out.println(sbc.step() + "\t- " + sbc);
			else
				System.out.println(sbc.step() + "\t- " + smc.step());
	}
}