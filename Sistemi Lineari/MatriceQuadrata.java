import static java.lang.Math.*;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.text.DecimalFormat;
import java.text.NumberFormat;


public class MatriceQuadrata
{
	protected double matrice[][];
	
	public MatriceQuadrata(int n) throws Exception
	{
		if(n < 1)
			throw new Exception();
		
		matrice = new double[n][n];
	}
	
	public int size()
	{
		return matrice.length;
	}
	
	public void set(int i, int j, double v)
	{
		matrice[i][j] = v;
	}
	
	public void add(int i, int j, double v)
	{
		matrice[i][j] = matrice[i][j] + v;
	}
	
	public void set(int i, int j, int v)
	{
		matrice[i][j] = (double) v;
	}
	
	public void add(int i, int j, int v)
	{
		matrice[i][j] = matrice[i][j] + v;
	}
	
	public double get(int i, int j)
	{
		return matrice[i][j];
	}
	
	public double norma() throws Exception
	{
		double norma = 0;
		for(int i = 0; i<this.size(); i++)
		{
			double somma = abs(matrice[i][0]);
			for(int j = 1; j<this.size(); j++)
				somma += abs(matrice[i][j]);
			norma = somma > norma ? somma : norma;
		}
		return norma;
	}
	
	public String toString() 
	{
		NumberFormat fmt = new DecimalFormat("#0.00"); 
		String s = "";
		int n = this.size();
		for(int i = 0; i<n; i++)
		{
			for(int j = 0; j<n; j++)
				s += fmt.format(matrice[i][j]) + "\t";
			s+= "\n";
		}
		return s;
	}
	
	public double determinante() throws Exception
    {
        if(matrice.length == 1)
            return matrice[0][0];
        if(matrice.length == 2)
            return matrice[0][0]*matrice[1][1] - matrice[1][0]*matrice[0][1];
		// Algoritmo di LaPlace
		double det = 0d;
		for(int i = 0; i < this.size(); i++)
			det += pow(-1,i+1+1) * matrice[i][0] * sottomatrice(i, 0).determinante();
		return det;
	}
	
	public MatriceQuadrata sottomatrice(int is, int js) throws Exception
	{
		List<Double> temp = new ArrayList<>();
		MatriceQuadrata sm = new MatriceQuadrata(this.size()-1);
		for(int i = 0; i<this.size(); i++)
			if(i == is)
				continue;
			else
				for(int j = 0; j<this.size(); j++)
					if(j == js)
						continue;
					else
						temp.add(matrice[i][j]);
				
		int i = 0, j = 0;
		for(double d : temp)
		{
			if(sm.size() - j == 0)
			{
				j = 0;
				i++;
			}
			sm.set(i, j, d);
			j++;
		}
		
		return sm;
	}
	
	public MatriceQuadrata trasposta() throws Exception
	{
		LinkedList<Double> temp = new LinkedList<>();
		MatriceQuadrata tmatrice = new MatriceQuadrata(this.size());
		for(int i = 0; i<this.size(); i++)
			for(int j = 0; j<this.size(); j++)
				temp.offer(matrice[i][j]);
		for(int j=0; j<this.size(); j++)
			for(int i = 0; i<this.size(); i++)
				tmatrice.set(i, j, temp.poll());
		return tmatrice;
	}
	
	public MatriceQuadrata matriceDeiCofattori() throws Exception
	{
		MatriceQuadrata mdc = new MatriceQuadrata(this.size());
		for(int i = 0; i<this.size(); i++)
			for(int j = 0; j<this.size(); j++)
				mdc.set(i, j, cofattore(i, j));
		return mdc;
	}
	
	private double cofattore(int i, int j) throws Exception
	{
		return pow(-1, j+i+1+1) * sottomatrice(i, j).determinante();
	}
	
	public MatriceQuadrata prodottoPerScalare(double s) throws Exception
	{
		MatriceQuadrata nm = new MatriceQuadrata(this.size());
		for(int i = 0; i<this.size(); i++)
			for(int j = 0; j<this.size(); j++)
				nm.set(i, j, s*matrice[i][j]);
		return nm;
	}
	
	public MatriceQuadrata matriceInversa() throws Exception
	{
		return matriceDeiCofattori().trasposta().prodottoPerScalare(1d/this.determinante());
	}
	
	public double condizionamento() throws Exception
	{
		if(this.size() == 1)
			return this.get(0, 0);
		return this.norma() * this.matriceInversa().norma();
	}
	
	public Vettore prodottoPerVettore(Vettore v) throws Exception
	{
		if(v.size() != this.size())
			throw new Exception();
		
		Vettore nv = new Vettore(v.size());
		for(int i = 0; i<v.size(); i++)
			for(int j = 0; j<v.size(); j++)
				nv.set(i, nv.get(i) + this.get(i, j) * v.get(j));
		return nv;
	}
	
	public MatriceQuadrata prodottoPerMatrice(MatriceQuadrata mq) throws Exception
	{
		if(mq.size() != this.size())
			throw new Exception();
		
		MatriceQuadrata nmq = new MatriceQuadrata(mq.size());
		for(int i = 0; i<mq.size(); i++)
			for(int j = 0; j<mq.size(); j++)
				for(int k = 0; k<mq.size(); k++)
					nmq.add(i, j, this.get(i,k) * mq.get(k, j) );
		return nmq;
	}
	
	public MatriceQuadrata Winograd(MatriceQuadrata mq) throws Exception //Da controllare
	{
		if((mq.size() != this.size()) || mq.size() % 2 != 0)
			throw new Exception();
		
		MatriceQuadrata nmq = new MatriceQuadrata(mq.size());
		for(int i = 0; i<size(); i++)
		{
		
	} 
}