public class E_Condizionamento
{
	private static MyInput mi = new MyInput();
	
	public static void main(String args[]) throws Exception
	{
		Hilbert h;
		while(true)
		{
			int n = mi.readInt("Inserisci n");
			try
			{
				h = new Hilbert(n);
				break;
			}
			catch(Exception e)
			{
				System.out.println("N non ammesso, riprova.");
			}
		}
		System.out.println(h);
		System.out.println("Calcola l'indice di condizionamento");
		mi.premiPerContinuare();
		System.out.println("Indice di condizionamento: " + h.condizionamento());
	}
}