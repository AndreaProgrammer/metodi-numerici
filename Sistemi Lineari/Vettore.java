import java.util.List;
import java.util.ArrayList;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Vettore
{
	private List<Double> vect;
	
	public Vettore(int n)
	{
		vect = new ArrayList<>(n);
		for(int i = 0; i<n; i++)
			vect.add(0d);
	}
	
	public Vettore(double... dv)
	{
		vect = new ArrayList<>(dv.length);
		for(double d : dv)
			vect.add(d);
	}
	
	public Vettore(int... iv)
	{
		vect = new ArrayList<>(iv.length);
		for(int i : iv)
			vect.add((double)i);
	}
	
	public void set(int i, double d)
	{
		vect.set(i, d);
	}
	
	public void add(int i, double d)
	{
		this.set(i, this.get(i) + d);
	}
	
	public void set(int i, int v)
	{
		this.set(i, (double) v);
	}
	
	public void add(int i, int v)
	{
		this.add(i, (double)v );
	}
	
	public double get(int i)
	{
		return vect.get(i);
	}
	
	public int size()
	{
		return vect.size();
	}
	
	public String toString()
	{
		NumberFormat fmt = new DecimalFormat("#0.00");
		String s = "";
		for(Double d : vect)
			s += fmt.format(d) + "\t";
		return s;
	}
	
	public double prodottoWinograd(Vettore v) throws Exception
	{
		if(this.size() != v.size())
			throw new Exception();
		if((this.size()) % 2 != 0)
			throw new Exception();
		
		double wx = 0, wy = 0, hxy = 0;

		for(int i = 0; i<(v.size())/2; i++)
			wx += this.get(2*i) * this.get(2*i+1);
		for(int i = 0; i<(v.size())/2; i++)
			wy += v.get(2*i) * v.get(2*i+1);
		for(int i = 0; i<(v.size())/2; i++)
			hxy += (this.get(2*i) + v.get(2*i+1)) * (this.get(2*i+1) + v.get(2*i));
		
		return hxy - wx - wy;
	}
}