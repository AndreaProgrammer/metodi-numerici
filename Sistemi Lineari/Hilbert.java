import static java.lang.Math.*;

public class Hilbert extends MatriceQuadrata
{
	public Hilbert(int n) throws Exception
	{
		super(n);
		fill(n);
	}
	
	private void fill(int n)
	{
		for(int i = 0; i<n; i++)
			for(int j = 0; j<n; j++)
				matrice[i][j] = 1.0d/(((double)(i+1)+(double)(j+1))-1.0d);
	}
}