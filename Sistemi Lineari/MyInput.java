import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MyInput
{	

	private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	public static int readInt(String message)
	{
			System.out.print(message + " : ");
			while(true)
			{
				try
				{
					return Integer.parseInt(br.readLine());
				}
				catch(Exception e)
				{
					System.out.print("Errore, reinserisci : ");
				}
			}
	}
	
	public static double readDouble(String message)
	{
			System.out.print(message + " : ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while(true)
			{
				try
				{
					return Double.parseDouble(br.readLine());
				}
				catch(Exception e)
				{
					System.out.print("Errore, reinserisci : ");
				}
			}
	}
	
	public static void premiPerContinuare()
	{ 
        System.out.println("Premi un tasto per continuare...");
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {}  
	}
}